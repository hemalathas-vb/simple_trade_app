import './App.css';
import {BrowserRouter,Switch,Route} from 'react-router-dom'
import index from './components/mainindex';
import oneBot from './Store/onebot'
import navbar from './components/navbar';
import cartitem from './Store/cartitem';

function App() {
  return (
    <div className="App"> 
      <BrowserRouter>
      <navbar/>

        <Switch>
          <Route exact path="/bots" component={index}/>
          <Route exact path="/cartitem" Store={cartitem}/>
          
        </Switch>
      </BrowserRouter>
      
    </div>
  );
}

export default App;
