import {useSelector} from 'react-redux'
const cartitem = () =>{
    const cartData = useSelector(state => state.cartData)
    return(
        <div  className="cartBots">
            {cartData.map(eachItem=>
                <div className="eachCartBot">
                    <div className="eachBot">
                        <p className="cartHead">{eachItem.bot}</p>
                        <div className="indexValue">
                            <p className="cartHead">Index value</p>
                            <p className="cartPercent">{eachItem['index-value']}</p>
                        </div>
                        <div className="cart">
                            <h1 className="cartHead">CAGR</h1>
                            <p className="cartPercent">{eachItem.cagr}%</p>
                        </div>
                        <div className="buttons">
                            <h1 className="cartHead cartQuantity">Quantity:{eachItem.count}</h1>
                        </div>
                    </div>
                </div>
            )}
        </div>
    )
}
export default cartitem