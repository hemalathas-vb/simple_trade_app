import { Link } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import addtocart from '/Store/cart_action/action'
import { useSelector } from 'react-redux'
import './index.css'
const onebot = (props)=>{
    const data = useSelector(state => state.data)
    const newData = data.filter(eachData=>eachData.id===id)
    const dispatch = useDispatch();

    return(
        <div className=" Class">
            <div className="eachBot">
                <div>
                    <h1 className="botName">{newData[0].bot}</h1>
                    <p className="cartPercent">{newData[0].description}</p>
                </div>
                <div className="indexValue">
                    <p className="cartHead">Index value</p>
                    <p className="cartPercent">{newData[0]['index-value']}</p>
                </div>
                <div className="cart">
                    <h1 className="cartHead">CAGR</h1>
                    <p className="cartPercent">{newData[0].cagr}%</p>
                </div>
                <div className="buttons">
                    <Link to={`/bots`}>
                        <button className="viewAlgo">Back</button>
                    </Link>
                    <button className="buy" onClick={()=>dispatch(addToCart(newData[0]))}>Buy</button>
                    
                </div>
            </div>
        </div>
)}
    

export default onebot