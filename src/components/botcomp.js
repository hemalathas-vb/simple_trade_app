import {Link} from 'react-router-dom'
import './index.css'
import { useDispatch } from 'react-redux'
import addtocart from '../Store/cart_action/action'
import removefromcart from '../Store/cart_action/remove'
import { useSelector } from 'react-redux'
const botcomp = () =>{
    const data = useSelector(state => state.data)
    const dispatch = useDispatch();
    return(
        <div className="mainBots">
            {data.map(eachData=>
                <div className="eachBot">
                    <h1 className="botName">{eachData.bot}</h1>
                    <div className="indexValue">
                        <p className="cartHead">Index value</p>
                        <p className="cartPercent">{eachData['index-value']}</p>
                    </div>
                    <div className="cart">
                        <h1 className="cartHead">CAGR</h1>
                        <p className="cartPercent">{eachData.cagr}%</p>
                    </div>
                    <div className="buttons">
                    <Link to={`/bots-details/${eachData.id}`}>
                        <button className="viewAlgo">VIEW ALGO</button>
                        </Link>
                        <button className="buy" onClick={()=>dispatch(addToCart(eachData))}>Buy</button>
                        
                    </div>
                </div>
            )}
        </div>
    )
}
export default botcomp