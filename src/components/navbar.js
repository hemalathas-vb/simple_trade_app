import './index.css'
import { useSelector } from "react-redux"
import { Link } from "react-router-dom"
const navbar = () =>{
    const CartValue = useSelector((state)=>state.noOfCartItems)
    return(
        <div>
            <h1 className="botsWelcome">Welcome To Trade App</h1>
            <div className="CartValue">
            <Link to="/bots"><p>DashBoard</p></Link>
            <p className="noOfItemsInCart">No of Items In Cart: {CartValue}</p>
            <Link to="/Store/cartitems">
                <div className="cartImageText">
                    <p className="cartText">OpenCart</p>
                </div>
            </Link>
        </div>
        </div>
    )
}
export default navbar